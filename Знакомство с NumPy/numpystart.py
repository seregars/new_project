import numpy as np

file = 'boston_houses.csv'
average_val = []
min_val = []
max_val = []
col_num = 0
while True:
    try:
        a = np.loadtxt(fname=file, skiprows=1, delimiter=',', usecols=col_num)  # берем столбцы из файла по порядку
        average_val.append(np.average(a))  # добавляем средние значения каждого столбца в список
        min_val.append(np.min(a))  # добавляем минимальные значения каждого столбца в список
        max_val.append(np.max(a))  # добавляем максимальные значения каждого столбца в список
        col_num += 1
    except ValueError:
        break
print(average_val)
print(min_val)
print(max_val)