import random

lst = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
       'w', 'x', 'y', 'z']  # создаем список
numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']  # создаем список цифр
lst.extend([i.upper() for i in lst])  # добавляем заглавные буквы в список
lst.extend(numbers)  # добавляем цифры в список


def passwords(symbols_count, password_count):  # функция генерации паролей
    x = 0  # счетчик паролей
    try:
        while x < int(password_count):  # цикл количества паролей
            password = []
            y = 0  # счетчик символов
            try:
                while y < int(symbols_count):  # цикл количества символов в пароле
                    password.append(random.choice(lst))  # рандомный выбор символа из списка
                    y += 1  # счетчик символов
                print("".join(password))  # вывод пароля на экран
            except ValueError:  # если ввели не число
                print('Количество символов должно быть целым числом')
                break
            x += 1  # счетчик паролей
    except ValueError:  # если ввели не число
        print('Количество паролей должно быть целым числом')


passwords(symbols_count=input('Введите количество символов в пароле: '),
          password_count=input('Введите количество паролей: '))
